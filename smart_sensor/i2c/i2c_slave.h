/* Эмуляция специфичного ведомого устройства шины I2C. Эмулируется
 * адресное пространство размером 256 байт (полное 8-битное адресное пространство),
 * которое может читать и писать ведущий шины по протоколу I2C.
 * На основании чтения/записи определяются дальнейшие действия ведомого устройства
 * и последующее обновление значений адресного пространства */

#ifndef I2C_SLAVE_H_
#define I2C_SLAVE_H_

#include "types.h"
#include "config.h"
#include <stdint.h>


/**** Адреса регистрового пространства ведомого устройства, которое эмулируется ****/

/* Адресное простанство состиоит из нескольких секций:
 * 	1) Секция данных с сенсоров - содержит информацию о сенсорах и считанные с них значения;
 * 	2) Секция конфигурации - содержит настройки платформы и позволяет конфигурировать ее;
 * 	3) Служебная секция - служебные команды.
 * Каждая секция в свою очередь может содержать однотипные блоки данных:
 * 	1) Секция данных сенсоров - вся состоит из однотипных блоков;
 * 	2) Секция конфигурации - состоит из специфичных полей и блоков конфигурации сенсоров;
 * 	3) Служебная секция - не содержит специфичных однотиипных блоков. */

// --- Секция данных сенсоров (состоит только из блоков данных сенсоров):
#define REGMAP_SENSORS_DATA_BASE				0x00		// Начало секции
// --- Секция конфигурации платформы (содержит подсекцию конфигурации сенсоров):
#define REGMAP_PLATFORM_CONFIG_BASE				0x9C		// Начало секции
#define REGMAP_PLATCON_CONFIG_CRC_REG			0x9C		//   \__ Контрольная сумма секции конфигурации
#define REGMAP_PLATCON_PLATFORM_UID_REG			0x9E		//   \__ Уникальный идентификатор платформы
#define REGMAP_PLATCON_ANALOG_SNS_AMNT_REG		0xA2		//   \__ Количество задействованных аналоговых каналов
#define REGMAP_PLATCON_DIGITAL_SNS_AMNT_REG		0xA3		//   \__ Количество задействованных цифровых каналов
#define REGMAP_PLATCON_SENSORS_CONFIG_BASE		0xA4		//   \__ Подсекция блоков конфигурации сенсоров
// --- Служебная секция:
#define REGMAP_SERVICE_BASE						0xEC		// Начало секции
#define REGMAP_SERVICE_CONFIG_ACCESS_REG		0xEC		//   \__ Смена режима работы
#define REGMAP_SERVICE_PLATFORM_RST				0xEE		//   \__ Регистр сброса устройства


// Сколько независимых каналов поддерживает протокол (на данный момент
// ограничено размером адресного пространства)
#define MAX_CHANNELS_PROTOCOL_SUPPORT			12



/**** Типы ****/

// Описатель структуры одного блока данных сенсора. Вся секция
// данных сенсоров в адресном пространстве состоит из таких блоков
typedef struct __attribute__ ((__packed__)) {
	uint32_t UID;		// Уникальный идентификатор сенсора на канале
	uint16_t TYPE;		// Тип сенсора на канале
	uint8_t STATUS;		// Статус-байт
	uint32_t DATA;		// RAW данные с сенсора
	uint16_t CRC16;		// Контрольная сумма
} sensor_data_blk_t;
#define SENSOR_DATA_BLOCK_SIZE		(sizeof(sensor_data_blk_t))

// Описатель структуры одного блока конфигурации сенсора. Эти блоки находятся
// в секции конфигурации
typedef struct __attribute__ ((__packed__)) {
	uint32_t UID;		// Уникальный идентификатор сенсора
	uint16_t TYPE;		// Тип сенсора на канале
} sensor_config_blk_t;
#define SENSOR_CONFIG_BLOCK_SIZE	(sizeof(sensor_config_blk_t))

// Описатель структуры секции конфигурации в адресном пространстве
typedef struct __attribute__ ((__packed__)) {
	uint16_t CONFIG_CRC16;					// Контрольная сумма блока конфигурации
	uint32_t PLATFORM_UID;					// Уникальный идентификатор платформы
	uint8_t ANALOG_SNS_AMNT;				// Количество аналоговых каналов сенсоров
	uint8_t DIGITAL_SNS_AMNT;				// Количество цифровых каналов сенсоров
	sensor_config_blk_t SENSOR_CONFIG[MAX_CHANNELS_PROTOCOL_SUPPORT];	// Конфигурация
} sensor_config_sect_t;
#define SENSOR_CONFIG_SECT_SIZE		(sizeof(sensor_config_sect_t))

// Описатель структуры служебной области
typedef struct __attribute__ ((__packed__)) {
	uint16_t CONFIG_ACCESS;		// Регистр доступа к конфигурации
	uint8_t PLATFORM_RST;		// Регистр сброса
} service_sect_t;
#define SERVICE_SECT_SIZE			(sizeof(service_sect_t))

// Описатель всего адресного пространства ведомого I2C смарт-датчика
typedef struct __attribute__ ((__packed__)) {

	/* Данные сенсоров */
	sensor_data_blk_t SENSOR[MAX_CHANNELS_PROTOCOL_SUPPORT];

	/* Конфигурация */
	sensor_config_sect_t CONFIG;

	/* Служебная секция */
	service_sect_t SERVICE;

	uint8_t __reserved[17];

} i2c_regmap_t;
#define REGMAP_LENGHT	(sizeof(i2c_regmap_t))



/**** Экспортируемые переменные ****/
extern volatile i2c_regmap_t I2C_regmap;
extern volatile flag_t I2C_regmap_mutex;


void I2C_init (void);



#endif /* I2C_SLAVE_H_ */
