/* Обеспечивает функционал устройства */

#include "i2c_slave.h"
#include "LPC11xx.h"
#include "xprintf.h"
#include "crc16.h"


volatile i2c_regmap_t I2C_regmap;
volatile flag_t I2C_regmap_mutex;


#define DEBUG_SENSOR_CONFIG_STORAGE

#ifdef DEBUG_SENSOR_CONFIG_STORAGE
/* Тестовая конфигурация */
const sensor_config_sect_t config_section_storage = {

		0x64c0,				// CRC16
		11112222,			// PLARFORM_ID
		SENSORS_ANALOG,		// 3 + 9 = 12
		//SENSORS_DIGITAL,
		8,
		{	//  UID, TYPE
				{10, 1},		// Канал 0
				{20, 2},		// Канал 1
				{30, 3},		// 2
				{40, 4},		// 3
				{50, 5},		// 4
				{60, 6},		// 5
				{70, 7},		// 6
				{80, 8},		// 7
				{90, 9},		// 8
				{100, 10},		// 9
				{110, 11},		// 10
				{120, 12}		// 11
		}
};
#endif


/*===================================================================================
 * Возвращает сконфигурированный адрес ведомого I2C, по которому его должен
 * адресовать ведущий шины
 *===================================================================================*/
static uint32_t I2C_get_slave_addr (void) {


	/* Настройка IOCON для селектора адреса I2C */
	LPC_IOCON->PIO3_0 &= ~(0x07 | (0x03 << 3));
	LPC_IOCON->PIO3_1 &= ~(0x07 | (0x03 << 3));
	LPC_IOCON->PIO3_2 &= ~(0x07 | (0x03 << 3));
	LPC_IOCON->PIO3_3 &= ~(0x07 | (0x03 << 3));
	LPC_IOCON->PIO3_4 &= ~(0x07 | (0x03 << 3));
	LPC_IOCON->PIO3_5 &= ~(0x07 | (0x03 << 3));

	return 0;
}


/*===================================================================================
 * Загружает конфигурацию платформы из памяти
 * dest - куда загружать конфигурацию
 * len - длина конфигурации
 * Возвращает ненулевое значение в случае ошибки чтения конфигурации
 *===================================================================================*/
static uint32_t I2C_load_config (volatile sensor_config_sect_t* dest) {

#ifdef DEBUG_SENSOR_CONFIG_STORAGE
	const volatile sensor_config_sect_t* src = &config_section_storage;
#else
#error "Specify config loading address"
#endif

	uint32_t sensors_amount = src->ANALOG_SNS_AMNT + src->DIGITAL_SNS_AMNT;
	uint32_t return_value = 0;
	uint16_t crc;

	xprintf("%u", REGMAP_LENGHT);

	for(uint32_t j = 0; j < 12; j++) {
		I2C_regmap.SENSOR[j].DATA = 10;
		I2C_regmap.SENSOR[j].CRC16 = 10;
		I2C_regmap.SENSOR[j].STATUS = 10;
		I2C_regmap.SENSOR[j].TYPE = 10;
//		I2C_regmap.SENSOR[j].UID = 10;
	}



	/* Проверяем количество сенсоров (сколько поддерживает платформа) */
	if( sensors_amount == 0 || sensors_amount > (SENSORS_ANALOG + SENSORS_DIGITAL) ) {
		/* Недопустимое количество */
		xprintf("Bad sensors amount: %u", sensors_amount);
		return_value = 1;

	} else {
		/* Количество сенсоров с конфига в норме */

		/* Загружаем конфигурацию в адресное пространство */
		*dest = *src;


		//dest->SENSOR_CONFIG[11].UID = 0;

		/* Подчищаем неиспользованные блоки конфигурации сенсоров */
		for(uint32_t i = 0; i < MAX_CHANNELS_PROTOCOL_SUPPORT; i++) {
			dest->SENSOR_CONFIG[i].TYPE = 0;
			dest->SENSOR_CONFIG[i].UID = 0;
		}

		/* Проверяем контрольную сумму считанной конфигурации */
		crc = crc16_ccitt((void*)&(dest->PLATFORM_UID),
				(uint32_t)&(dest->SENSOR_CONFIG[sensors_amount]) - (uint32_t)&(dest->PLATFORM_UID) );

		if(crc != dest->CONFIG_CRC16) return_value = 1;
	}

	return return_value;
}




/*===================================================================================
 * Инициализация аппаратного I2C интерфейса в режиме ведомого.
 * Прерывания от интерфейса и прием не разрешаются
 *===================================================================================*/
void I2C_init (void) {

	/* Загрузка конфигурации */

	/* Тактирование на I2C и GPIO */
	LPC_SYSCON->SYSAHBCLKCTRL |= ((1 << 5) | (1 << 6));

	/* Настройка задействованных интерфейсом пинов */
	LPC_IOCON->PIO0_3 &= ~(0x07 | (0x03 << 3));		// I2C activity wakeup, GPIO, No PU/PD
	LPC_IOCON->PIO0_4 &= ~(0x07 | (0x03 << 8));		// I2C SCL, open drain, standard I2C mode
	LPC_IOCON->PIO0_4 |= 0x01;
	LPC_IOCON->PIO0_5 &= ~(0x07 | (0x03 << 8));		// I2C SDA, open drain, standard I2C mode
	LPC_IOCON->PIO0_5 |= 0x01;


	xprintf("SENSOR_DATA_BLOCK_SIZE: %u\n"
			"SENSOR_CONFIG_BLOCK_SIZE: %u\n"
			"REGMAP_LENGHT: %u\n",
			SENSOR_DATA_BLOCK_SIZE,
			SENSOR_CONFIG_BLOCK_SIZE,
			REGMAP_LENGHT);

	I2C_load_config(&(I2C_regmap.CONFIG));
}






