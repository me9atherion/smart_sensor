/* Вспомогательные типы */

#ifndef TYPES_H_
#define TYPES_H_

typedef enum {CLEAR = 0, SET = !CLEAR} flag_t;		// Флаг

#endif /* TYPES_H_ */
