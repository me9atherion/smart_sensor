/* Библиотечка для формирования задержек с помощью системного таймера */

#ifndef SYSTICK_H_
#define SYSTICK_H_

#include <stdint.h>


/* Компромисс между нагрузкой на ядро (частота обработки прерывания от системного таймера) и
 * разрешающей способностью таймера. По этой константе инициализируется модуль и вычисляются
 * задержки в милисекундах */
#define MILIS_IN_TICK		10		// Сколько миллисекунд в одном тике системного таймера


/* Прототипы */
uint32_t systickInit (void);
void systickDeInit (void);
uint32_t systickGetTicks(void);
uint32_t systickCalculateDelay_ms (uint32_t prev_ticks);
void systickWait_ms (uint32_t milis);

#endif /* SYSTICK_H_ */
