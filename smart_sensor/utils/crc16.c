
#include "crc16.h"


/* CRC16 value calculation */
uint16_t crc16_ccitt(const void *src, uint32_t len) {

	const uint8_t *data = src;
	uint8_t i;
    uint16_t crc = 0x0000;

    while (len--) {
        i = crc >> 8 ^ *data++;
        i ^= i >> 4;
        crc = (crc << 8) ^ ((uint16_t)(i << 12)) ^ ((uint16_t)(i <<5)) ^ ((uint16_t)i);
    }
    return crc;
}
