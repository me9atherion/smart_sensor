/* CRC-16 (X-Modem) calculation */

#ifndef CRC16_H_
#define CRC16_H_


#include "stdint.h"


/* CRC16 from byte array of specified lenght. Init CRC value = 0x0000.
 * CRC algorythm: CRC16-CCITT (X-Modem) */
uint16_t crc16_ccitt (const void* data, uint32_t len);

#endif /* CRC16_H_ */
