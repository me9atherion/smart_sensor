/* Модуль для определения источника HARDFAULT */

#include <stdint.h>
#include "LPC11xx.h"
#include "xprintf.h"


/*===================================================================================
 * Обработчик HardFault. Обеспечивает трейсбек
 *===================================================================================*/
__attribute__ ((section(".after_vectors")))
void HardFault_Handler(void) {

	xputs_ts("Hard Fault Exception!");
	for(;;);
}


