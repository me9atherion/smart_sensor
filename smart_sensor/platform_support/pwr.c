
#include "LPC11xx.h"
#include "pwr.h"
#include "atomic.h"


volatile uint8_t just_waked = 0;		// Флаг просыпания
volatile uint32_t __sysahbclkctrl;		// Хранилище тактирования периферии до засыпания
volatile uint32_t __pdruncfg;			// Хранилище питания периферии до засыпания
volatile uint32_t __nvic_iser;			// Хранилище активных прерываний до засыпания


/*===================================================================================
 * Перенастройка портов перед спячкой
 *===================================================================================*/
static void down_gpio (void) {
	__NOP();
}


/*===================================================================================
 * Перенастройка портов по выходу из спячки
 *===================================================================================*/
static void up_gpio (void) {
	__NOP();
}


/*==================================================================================
 * Вывод сигнала тактирования на пин CLKOUT (для отладки)
 *==================================================================================*/
void pwr_clkout(uint8_t enable, enum clkout source) {

	//make CLKOUT
	enable &= 0x01;
	LPC_IOCON->PIO0_1 &= ~(0x07);
	LPC_IOCON->PIO0_1 |= enable | 0x8; //set PIO0_1 as CLKOUT
	if(enable){
		LPC_SYSCON->CLKOUTCLKSEL = source; /* main clk */
		LPC_SYSCON->CLKOUTUEN = 0x00;
		LPC_SYSCON->CLKOUTUEN = 0x01;
		LPC_SYSCON->CLKOUTDIV = 1;
		while(!(LPC_SYSCON->CLKOUTUEN&0x01)); /* wait for clock switched */
		LPC_SYSCON->CLKOUTDIV = LPC_SYSCON->SYSAHBCLKDIV;
	}
}


/*===========================================================================
 * Переключение на тактирование от кварцевого резонатора c PLL. Предполагается
 * работа кварцевого генератора от кварца 12МГц.
 *===========================================================================*/
void pwr_switch_to_48MHz (void) {

	/* Enable System osc and PLL */
	LPC_SYSCON->PDRUNCFG &= ~( (1 << 5) | (1 << 7) );

	LPC_SYSCON->SYSOSCCTRL = 0;			// 12Mhz crystal resonator
	LPC_SYSCON->SYSPLLCLKSEL = 1;		// System osc as PLL input
	LPC_SYSCON->SYSPLLCLKUEN  = 0x01;	// Toggle
	LPC_SYSCON->SYSPLLCLKUEN  = 0x00;
	LPC_SYSCON->SYSPLLCLKUEN  = 0x01;
	while (!(LPC_SYSCON->SYSPLLCLKUEN & 0x01));

	LPC_SYSCON->SYSPLLCTRL = 0x23;		// Set PPL out to 48MHz
	while (!(LPC_SYSCON->SYSPLLSTAT & 0x01));

	LPC_SYSCON->MAINCLKSEL = 0x03;		// Switch to PLL out
	LPC_SYSCON->MAINCLKUEN = 0x00;
	LPC_SYSCON->MAINCLKUEN = 0x01;
	while(!(LPC_SYSCON->MAINCLKUEN & 0x01));
}


/*===========================================================================
 * Переключение на тактирование от встроенного RC генератора 12МГц.
 * PLL и кварцевый генератор отключаются
 *===========================================================================*/
void pwr_switch_to_12MHz_RC (void) {

	/* Switch to IRC oscillator */
	LPC_SYSCON->MAINCLKSEL = 0x00;
	LPC_SYSCON->MAINCLKUEN = 0x00;
	LPC_SYSCON->MAINCLKUEN = 0x01;
	while(!(LPC_SYSCON->MAINCLKUEN & 0x01));

	/* Disable System osc and PLL */
	LPC_SYSCON->PDRUNCFG |= (1 << 5) | (1 << 7);
}


/*===========================================================================
 * Переключение на тактирование от встроенного низкочастотного генератора
 * вачдога. PLL и кварцевый генератор отключаются
 *===========================================================================*/
void pwr_switch_to_9KHz (void) {

	/* Switch to WDT oscillator */
	LPC_SYSCON->MAINCLKSEL = 0x02;
	LPC_SYSCON->MAINCLKUEN = 0x00;
	LPC_SYSCON->MAINCLKUEN = 0x01;
	while(!(LPC_SYSCON->MAINCLKUEN & 0x01));

	/* Disable System osc and PLL */
	LPC_SYSCON->PDRUNCFG |= (1 << 5) | (1 << 7);
}


/*===========================================================================
 * Инит генератора вачдога для спящего режима и для самого вачдога.
 * Таймаут по умолчанию равен 2 с
 *===========================================================================*/
void wdt_init (void) {

	/* Enable clock to WDT */
	LPC_SYSCON->SYSAHBCLKCTRL |= (1 << 15);

	/* Enable WDT power */
	LPC_SYSCON->PDRUNCFG &= ~(1 << 6);

	/* WTD OSC frequency = 9375Hz (0.6MHz/64) -
	 * самая низкая частота генератора */
	LPC_SYSCON->WDTOSCCTRL = 0x3F;

	/* Divider for WDTCLK (0 - WDTCLK disabled) */
	LPC_SYSCON->WDTCLKDIV = 0x01;

	/* Select clock source for WDT */
	LPC_SYSCON->WDTCLKSEL = 0x02; /* WDT osc */
	LPC_SYSCON->WDTCLKUEN = 0x00; /* Toggle register to switch clock source */
	LPC_SYSCON->WDTCLKUEN = 0x01;
	while(!(LPC_SYSCON->WDTCLKUEN & 0x01)); /* Wait for clock switched */

	/* Диапазон и расчет таймаута вачдога:
	 *
	 * F_WDCLK = ~9375 Hz 	- тактирование вачдога
	 * TCreg_v = 255..2^24	- диапазон значений регистра TC (Time Constant)
	 * Timeout [s] = T_WDCLK * 4 * TCreg_v =
	 * = (1/F_WDCLK) * 4 * TCreg_v
	 *
	 * TCreg_v = 255, Timeout = 0.1088 s
	 * TCreg_v = 2^24, Timeout = 7158 s = 119 min = 1.99 h
	 *
	 * TCreg_v = ( Timeout[s] * F_WDCLK ) / 4
	 */

	/* Init WDT mode. WDT will reset system after timeout if not feeded */
	LPC_WDT->TC = 4687; /* Default timeout is 2 secons */
	LPC_WDT->MOD |= (1 << 0) | (1 << 1);	/* WDEN & WDRESET (further WDT reset source disabling is blocked!) */

	/* После инициализации WDT его нужно перезагрузить чтобы он начал
	 * считать таймаут. До первой перезагрузки он остановлен.
	 * Отключение вачдога после инициализации уже невозможно
	 * а его таймер заблокирован */
}


/*===========================================================================
 * Перезагрузка вачдога. Загружается предыдущий таймаут
 *===========================================================================*/
__attribute__ ((always_inline)) inline void wdt_feed (void) {
	/* Запись этой последовательности перезагружает
	 * WDT. Запись должна быть атомарной */
	ATOMIC_BLOCK_RESTORATE {
		LPC_WDT->FEED = 0xAA;
		LPC_WDT->FEED = 0x55;
	}
}


/*===========================================================================
 * Меняет таймаут и перезагружает вачдог. После этого он начинает отсчитывать
 * заново новый таймаут
 *===========================================================================*/
void wdt_new_timeout (uint16_t seconds) {

	/* Вычисляем значение регистра TC для заданного таймаута в секундах */
	uint32_t wdt_tc_reg = ((uint32_t) seconds * 9375) / 4;
	/* Пишем новое значение */
	LPC_WDT->TC = wdt_tc_reg;
	/* Перезагружаем */
	wdt_feed();
}


/*===========================================================================
 * Конфигурирует таймер CT32B0 на генерирование прерывания по ножке
 * PIO0_1 для выхода из Deep-sleep
 *===========================================================================*/
static void pwr_set_wakeup_timer (uint16_t _10ms) {

	/* PIO0_1 - вход стартовой логики пробуждения девайса. Этот вход
	 * дергает таймер для организации запланированого пробуждения */
	LPC_IOCON->PIO0_1 &= ~( 0x07 | (0x03 << 3) );
	LPC_IOCON->PIO0_1 |= 0x02 | (0x01 << 3);	// CT32B0_MAT2 + pulldown (на всякий случай)

	LPC_SYSCON->SYSAHBCLKCTRL |= (1 << 9);	// Тактирование CT32B0
	LPC_TMR32B0->TCR |= (1 << 1);			// Reset
	LPC_TMR32B0->IR = 0xff;
	LPC_TMR32B0->CTCR = 0;					// Timer mode
	LPC_TMR32B0->PR = 0;					// * Set some value if prescaling of timer is needed
	LPC_TMR32B0->MCR = 0x6 << 6;			// Reset and stop on MR2  //interrupt
	LPC_TMR32B0->MR2 = 93*_10ms;			// Match value!!!! sets delay!!!
	LPC_TMR32B0->EMR = 2 << 8;				// Set 0 to TMR32B0_MAT2 pin on match //set 1
	//LPC_TMR32B0->TCR = 1;					// Start
}


/*===========================================================================
 * Перевод девайса в Deep-sleep XXX
 *===========================================================================*/
void pwr_deepsleep (uint16_t _100_ms) {

	/* Сохраняем разрешенные прерывания и запрещаем все прерывания
	 * от периферии в NVIC */
	__nvic_iser = NVIC->ISER[0];
	NVIC->ICER[0] = 0xFFFFFFFF;

	/* Deep-sleep после инструкции WFI */
	LPC_PMU->PCON = (1 << 11);		// Запрет Deep-powerdown режима со сбросом флагов
	LPC_PMU->PCON |= (1 << 8);		// SLEEPFLAG и DPDFLAG
	SCB->SCR |= (1 << 2);			// Deep-sleep вместо простого Sleep

	/* Питание периферии в спячке */
	//LPC_SYSCON->PDSLEEPCFG = 0x18BF; /* WDT on, BOD off */
	LPC_SYSCON->PDSLEEPCFG = 0x18B7; /* WDT on, BOD on */

	/* Cтатус питания до засыпания */
	__pdruncfg = LPC_SYSCON->PDRUNCFG;
	/* Питание периферии после выхода из спячки такое же как до входа */
	LPC_SYSCON->PDAWAKECFG = LPC_SYSCON->PDRUNCFG;

	/* Перенастраиваем вачдог */
	wdt_new_timeout(60*30);

	/* Ставим флаг будущего просыпания */
	just_waked = 1;

	down_gpio();

	/* Перенастраиваем таймер. После настройки он остановлен */
	pwr_set_wakeup_timer(_100_ms*10);

	/* Тактирование до засыпания */
	__sysahbclkctrl = LPC_SYSCON->SYSAHBCLKCTRL;

	/* Оставляем тактировать лишь нужные участки */
	LPC_SYSCON->SYSAHBCLKCTRL =
				(1 << 0) |	/* SYS */
				(1 << 1) |	/* ROM */
				(1 << 2) |	/* RAM */
				(1 << 3) |	/* FLASHREG */
				(1 << 4) |	/* FLASHARRAY */
				(1 << 9) |	/* CT32B0 - источник просыпания по таймауту (через выход на пин PIO0_1) */
				(1 << 15);	/* WDT - источник тактирования в спячке */

	NVIC_DisableIRQ(WAKEUP1_IRQn);
	NVIC_DisableIRQ(WAKEUP3_IRQn);
	NVIC_DisableIRQ(WAKEUP11_IRQn);

	/* Rising Edge на пинах PIO0_1 & PIO0_3, по PIO0_11 остается Falling Edge */
	LPC_SYSCON->STARTAPRP0 = (1 << 1) | (1 << 3);

	/* Сбрасываем стартовую логику */
	LPC_SYSCON->STARTRSRP0CLR = (1 << 1) | (1 << 3) | (1 << 11);

	/* Разрешаем стартовую логику на всех трех пинах PIO0_1, PIO0_3, PIO0_11 */
	LPC_SYSCON->STARTERP0 = (1 << 1) | (1 << 3) | (1 << 11);

	NVIC_ClearPendingIRQ(WAKEUP1_IRQn);
	NVIC_ClearPendingIRQ(WAKEUP3_IRQn);
	NVIC_ClearPendingIRQ(WAKEUP11_IRQn);
	NVIC_EnableIRQ(WAKEUP1_IRQn);
	NVIC_EnableIRQ(WAKEUP3_IRQn);
	NVIC_EnableIRQ(WAKEUP11_IRQn);

	/* Переключаем тактирование ядра на осцилятор вачдога
	 * (единственный источник тактирования в режиме Deep-sleep) */
	LPC_SYSCON->MAINCLKSEL = 0x02;
	LPC_SYSCON->MAINCLKUEN = 0x00;
	LPC_SYSCON->MAINCLKUEN = 0x01;
	while(!(LPC_SYSCON->MAINCLKUEN & 0x01));

	/* Стартуем таймер */
	LPC_TMR32B0->TCR = 1;

	/* Синхронизация, а затем сон */
	__DSB();
	__WFI();

	/* ------------------- Сон --------------------- */

	/* Просыпание. Сейчас должны отработать обработчики прерываний по пробуждению */
}


/*==================================================================================
 * Процедура вывода девайса из режима DeepSleep XXX
 *==================================================================================*/
void pwr_deepsleep_restore (void) {

	/* Запрещаем все прерывания (возможны вложенные прерывания)
	 * при восстановлении из спячки */
	ATOMIC_BLOCK_RESTORATE {

		if(just_waked) {

			LPC_SYSCON->SYSAHBCLKCTRL = __sysahbclkctrl;	// Восстанавливаем тактирование
			LPC_TMR32B0->TCR = 0; 							// Останавливаем таймер вывода из сна
			LPC_SYSCON->PDRUNCFG = __pdruncfg;				// Удостоверились что питание также восстановлено
			LPC_SYSCON->PDRUNCFG &= ~((1 << 0) | (1 << 1)); // (особенно у RC осциллятора)

			LPC_SYSCON->STARTRSRP0CLR = (1 << 1) | (1 << 3) | (1 << 11);
			LPC_SYSCON->STARTERP0 = 0;						// Запрещаем работу всей стартовой логики

			/* Восстанавливаем таймаут вачдога*/
			wdt_new_timeout(2);
			__NOP();
			__NOP();
			__NOP();
			__NOP();
			__NOP();
			__NOP();

			/* Переключаем частоту тактирования обратно на RC осциллятор */
			pwr_switch_to_12MHz_RC();
//			LPC_SYSCON->MAINCLKSEL = 0x00;
//			LPC_SYSCON->MAINCLKUEN = 0x00;
//			LPC_SYSCON->MAINCLKUEN = 0x01;
//			while(!(LPC_SYSCON->MAINCLKUEN & 0x01));
//			LPC_SYSCON->PDRUNCFG |= (1 << 5) | (1 << 7);

			/* Восстанавливаем прерывания */
			NVIC->ISER[0] = __nvic_iser;

			NVIC_ClearPendingIRQ(WAKEUP1_IRQn);
			NVIC_DisableIRQ(WAKEUP1_IRQn);
			NVIC_ClearPendingIRQ(WAKEUP2_IRQn);
			NVIC_DisableIRQ(WAKEUP3_IRQn);
			NVIC_ClearPendingIRQ(WAKEUP3_IRQn);
			NVIC_DisableIRQ(WAKEUP11_IRQn);

			/* Предотвращаем повторное выполнение этой процедуры, если
			 * будет сразу несколько источников просыпания */
			just_waked = 0;
		}
	}
}
