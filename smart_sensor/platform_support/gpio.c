/* GPIO. Содержит функции для управления ключами нагревателей,
 * каналов измерения и т.п. */

#include "LPC11xx.h"			/* LPC11xx Peripheral Registers */
#include "gpio.h"
#include "atomic.h"

/* Идентификатор пина в формате порт/пин */
typedef struct __attribute__ ((packed)) {
	const uint8_t port_n;
	const uint8_t pin_n;
} gpio_conf_t;


/* Определение доступного массива портов на платформе */
__IO LPC_GPIO_TypeDef * const GPIO_port[] =
{
		LPC_GPIO0,
		LPC_GPIO1,
		LPC_GPIO2,
		LPC_GPIO3
};


/* Структура пинов для инициализации и быстрого использования */
const struct __attribute__ ((packed)) {

	const gpio_conf_t led[LEDS_CH_AMOUNT];
	const gpio_conf_t uhtr[EN_UHTR_CH_AMOUNT];
	const gpio_conf_t usns[EN_USNS_CH_AMOUNT];
	const gpio_conf_t en_avcc;

} GPIO_config = {

		{ // LED[]
				{LED1_PORT, LED1_PINn},
				{LED2_PORT, LED2_PINn},
				{LED3_PORT, LED3_PINn}
		},
		{ // UHTR[]
				{EN_UHTR1_PORTn, EN_UHTR1_PINn},
				{EN_UHTR2_PORTn, EN_UHTR2_PINn},
				{EN_UHTR3_PORTn, EN_UHTR3_PINn}
		},
		{ // USNS[]
				{EN_USNS1_PORTn, EN_USNS1_PINn},
				{EN_USNS2_PORTn, EN_USNS2_PINn},
				{EN_USNS3_PORTn, EN_USNS3_PINn}
		},
		{EN_AVCC_PORTn, EN_AVCC_PINn}	// EN_AVCC (для групповой инициализации)
};


/*===================================================================================
 *
 *===================================================================================*/
//void PIOINT0_IRQHandler(void)
//{
//	uint32_t regVal;
//
//	regVal = GPIOIntStatus( PORT0, 1 );
//	if ( regVal )
//	{
//
//		GPIOIntClear( PORT0, 1 );
//	}
//}




/*===================================================================================
 * Инициализация GPIO платформы
 *===================================================================================*/
void GPIO_Init(void) {

	/* Enable AHB clock to the GPIO domain. */
	LPC_SYSCON->SYSAHBCLKCTRL |= (1<<6);

#ifdef __JTAG_DISABLED
	LPC_IOCON->JTAG_TDO_PIO1_1  &= ~0x07;
	LPC_IOCON->JTAG_TDO_PIO1_1  |= 0x01;
#endif

	/* Настройка IOCON для светодиодов */
	LPC_IOCON->PIO1_9 &= ~(0x07 | (0x03 << 3));			// LED1, GPIO, No PU/PD
	LPC_IOCON->PIO1_10 &= ~(0x07 | (0x03 << 3));		// LED2, GPIO, No PU/PD
	LPC_IOCON->R_PIO0_11 &= ~(0x07 | (0x03 << 3));		// LED3, GPIO, No PU/PD
	LPC_IOCON->R_PIO0_11 |= 0x01;

	/* Настройка IOCON для аналоговых ключей. Переводим подтяжки в PD для
	 * чистого перевода этих пинов в режим выхода */
	LPC_IOCON->PIO2_4 &= ~(0x07 | (0x03 << 3));			// EN_UHTR1, GPIO, No PU/PD
	LPC_IOCON->PIO2_4 |= (0x01 << 3);
	LPC_IOCON->PIO2_5 &= ~(0x07 | (0x03 << 3));			// EN_USNS1, GPIO, No PU/PD
	LPC_IOCON->PIO2_5 |= (0x01 << 3);
	LPC_IOCON->PIO2_6 &= ~(0x07 | (0x03 << 3));			// EN_UHTR2, GPIO, No PU/PD
	LPC_IOCON->PIO2_6 |= (0x01 << 3);
	LPC_IOCON->PIO2_7 &= ~(0x07 | (0x03 << 3));			// EN_USNS2, GPIO, No PU/PD
	LPC_IOCON->PIO2_7 |= (0x01 << 3);
	LPC_IOCON->PIO2_8 &= ~(0x07 | (0x03 << 3));			// EN_UHTR3, GPIO, No PU/PD
	LPC_IOCON->PIO2_8 |= (0x01 << 3);
	LPC_IOCON->PIO2_9 &= ~(0x07 | (0x03 << 3));			// EN_USNS3, GPIO, No PU/PD
	LPC_IOCON->PIO2_9 |= (0x01 << 3);
	LPC_IOCON->PIO2_11 &= ~(0x07 | (0x03 << 3));		// EN_AVCC, GPIO, No PU/PD
	LPC_IOCON->PIO2_11 |= (0x01 << 3);

	/* Конфигурируем их как выходы */
	const gpio_conf_t* config_struct_ptr = (gpio_conf_t *) &GPIO_config;
	for(uint32_t i = (sizeof(GPIO_config) / sizeof(gpio_conf_t));
			i > 0;
			i--) {
		GPIO_SetDir(config_struct_ptr->port_n, config_struct_ptr->pin_n, 1);
		GPIO_SetValue(config_struct_ptr->port_n, config_struct_ptr->pin_n, 0);
		config_struct_ptr++;
	}
}


/*===================================================================================
 * Управление нагревателем аналогового сенсора на соответствующем канале:
 * ch - канал 0, 1, ...
 * state - новое состояние на канале (1 - вкл., 0 - выкл.)
 *===================================================================================*/
void GPIO_UHTRctrl(uint32_t ch, uint32_t state) {

	if(ch > (EN_UHTR_CH_AMOUNT - 1)) return;

	const gpio_conf_t* uhtr_ptr = (gpio_conf_t*) &GPIO_config.uhtr;
	GPIO_SetValue(uhtr_ptr[ch].port_n, uhtr_ptr[ch].pin_n, state);
}


/*===================================================================================
 * Управление подачей питания на чувствительный элемент сенсора сооттветствующего
 * канала:
 * ch - канал 0, 1, ...
 * state - новое состояние на канале (1 - вкл., 0 - выкл.)
 *===================================================================================*/
void GPIO_USNSctrl (uint32_t ch, uint32_t state) {

	if(ch > (EN_USNS_CH_AMOUNT - 1)) return;

	const gpio_conf_t* usns_ptr = (gpio_conf_t*) &GPIO_config.usns;
	GPIO_SetValue(usns_ptr[ch].port_n, usns_ptr[ch].pin_n, state);
}


/*===================================================================================
 * Управление светодиодами:
 * ch - канал 0, 1, ...
 * state - новое состояние на канале (1 - вкл., 0 - выкл.)
 *===================================================================================*/
void GPIO_LEDctrl (uint32_t ch, uint32_t state) {

	if(ch > (LEDS_CH_AMOUNT - 1)) return;

	const gpio_conf_t* led_ptr = (gpio_conf_t*) &GPIO_config.led;
	GPIO_SetValue(led_ptr[ch].port_n, led_ptr[ch].pin_n, state);
}


/*****************************************************************************
** Function name:		GPIOSetDir
**
** Descriptions:		Set the direction in GPIO port
**
** parameters:			port num, bit position, direction (1 out, 0 input)
** Returned value:		None
**
*****************************************************************************/
void GPIO_SetDir( uint32_t portNum, uint32_t bitPosi, uint32_t dir )
{
  /* if DIR is OUT(1), but GPIOx_DIR is not set, set DIR
  to OUT(1); if DIR is IN(0), but GPIOx_DIR is set, clr
  DIR to IN(0). All the other cases are ignored.
  On port3(bit 0 through 3 only), no error protection if
  bit value is out of range. */
//  switch ( portNum )
//  {
//	case PORT0:
//	  if ( !(LPC_GPIO0->DIR & (0x1<<bitPosi)) && (dir == 1) )
//		LPC_GPIO0->DIR |= (0x1<<bitPosi);
//	  else if ( (LPC_GPIO0->DIR & (0x1<<bitPosi)) && (dir == 0) )
//		LPC_GPIO0->DIR &= ~(0x1<<bitPosi);
//	break;
// 	case PORT1:
//	  if ( !(LPC_GPIO1->DIR & (0x1<<bitPosi)) && (dir == 1) )
//		LPC_GPIO1->DIR |= (0x1<<bitPosi);
//	  else if ( (LPC_GPIO1->DIR & (0x1<<bitPosi)) && (dir == 0) )
//		LPC_GPIO1->DIR &= ~(0x1<<bitPosi);
//	break;
//	case PORT2:
//	  if ( !(LPC_GPIO2->DIR & (0x1<<bitPosi)) && (dir == 1) )
//		LPC_GPIO2->DIR |= (0x1<<bitPosi);
//	  else if ( (LPC_GPIO2->DIR & (0x1<<bitPosi)) && (dir == 0) )
//		LPC_GPIO2->DIR &= ~(0x1<<bitPosi);
//	break;
//	case PORT3:
//	  if ( !(LPC_GPIO3->DIR & (0x1<<bitPosi)) && (dir == 1) )
//		LPC_GPIO3->DIR |= (0x1<<bitPosi);
//	  else if ( (LPC_GPIO3->DIR & (0x1<<bitPosi)) && (dir == 0) )
//		LPC_GPIO3->DIR &= ~(0x1<<bitPosi);
//	break;
//	default:
//	  break;
//  }
//  return;

	if(dir) {
		/* Выход */
		GPIO_port[portNum]->DIR |= (1 << bitPosi);

	} else {
		/* Вход */
		GPIO_port[portNum]->DIR &= ~(1 << bitPosi);
	}
}


/*****************************************************************************
** Function name:		GPIOSetValue
**
** Descriptions:		Set/clear a bitvalue in a specific bit position
**						in GPIO portX(X is the port number.)
**
** parameters:			port num, bit position, bit value
** Returned value:		None
**
*****************************************************************************/
void GPIO_SetValue( uint32_t portNum, uint32_t bitPosi, uint32_t bitVal )
{
	// Check bitVal is a binary value - 0 or 1
//	if (bitVal <2 )
//	{
	/* The MASKED_ACCESS registers give the ability to write to a specific bit
	 * (or bits) within the GPIO data register. See the LPC11/13 user manual
	 * for more details.
	 *
	 * (1<<bitPosi) gives us the MASKED_ACCESS register specific to the bit
	 * that is being requested to be set or cleared.
	 *
	 * (bitVal<<bitPosi) will be either be 0 or will contain a 1 in the
	 * appropriate bit position that matches the MASKED_ACCESS register
	 * being written to.
	 */
//	  switch ( portNum )
//	  {
//		case PORT0:
//				 LPC_GPIO0->MASKED_ACCESS[(1<<bitPosi)] = (bitVal<<bitPosi);
//		break;
//		case PORT1:
//				 LPC_GPIO1->MASKED_ACCESS[(1<<bitPosi)] = (bitVal<<bitPosi);
//		break;
//		case PORT2:
//				 LPC_GPIO2->MASKED_ACCESS[(1<<bitPosi)] = (bitVal<<bitPosi);
//		break;
//		case PORT3:
//				 LPC_GPIO3->MASKED_ACCESS[(1<<bitPosi)] = (bitVal<<bitPosi);
//		break;
//		default:
//		  break;
//	  }
//	}
//  return;

	if(bitVal) bitVal = 1;
	GPIO_port[portNum]->MASKED_ACCESS[(1 << bitPosi)] = (bitVal << bitPosi);
}


/*****************************************************************************
** Function name:		GPIOSetInterrupt
**
** Descriptions:		Set interrupt sense, event, etc.
**						edge or level, 0 is edge, 1 is level
**						single or double edge, 0 is single, 1 is double
**						active high or low, etc.
**
** parameters:			port num, bit position, sense, single/doube, polarity
** Returned value:		None
**
*****************************************************************************/
void GPIO_SetInterrupt( uint32_t portNum, uint32_t bitPosi, uint32_t sense,
			uint32_t single, uint32_t event )
{
//  switch ( portNum )
//  {
//	case PORT0:
//	  if ( sense == 0 )
//	  {
//		LPC_GPIO0->IS &= ~(0x1<<bitPosi);
//		/* single or double only applies when sense is 0(edge trigger). */
//		if ( single == 0 )
//		  LPC_GPIO0->IBE &= ~(0x1<<bitPosi);
//		else
//		  LPC_GPIO0->IBE |= (0x1<<bitPosi);
//	  }
//	  else
//	  	LPC_GPIO0->IS |= (0x1<<bitPosi);
//	  if ( event == 0 )
//		LPC_GPIO0->IEV &= ~(0x1<<bitPosi);
//	  else
//		LPC_GPIO0->IEV |= (0x1<<bitPosi);
//	break;
// 	case PORT1:
//	  if ( sense == 0 )
//	  {
//		LPC_GPIO1->IS &= ~(0x1<<bitPosi);
//		/* single or double only applies when sense is 0(edge trigger). */
//		if ( single == 0 )
//		  LPC_GPIO1->IBE &= ~(0x1<<bitPosi);
//		else
//		  LPC_GPIO1->IBE |= (0x1<<bitPosi);
//	  }
//	  else
//	  	LPC_GPIO1->IS |= (0x1<<bitPosi);
//	  if ( event == 0 )
//		LPC_GPIO1->IEV &= ~(0x1<<bitPosi);
//	  else
//		LPC_GPIO1->IEV |= (0x1<<bitPosi);
//	break;
//	case PORT2:
//	  if ( sense == 0 )
//	  {
//		LPC_GPIO2->IS &= ~(0x1<<bitPosi);
//		/* single or double only applies when sense is 0(edge trigger). */
//		if ( single == 0 )
//		  LPC_GPIO2->IBE &= ~(0x1<<bitPosi);
//		else
//		  LPC_GPIO2->IBE |= (0x1<<bitPosi);
//	  }
//	  else
//	  	LPC_GPIO2->IS |= (0x1<<bitPosi);
//	  if ( event == 0 )
//		LPC_GPIO2->IEV &= ~(0x1<<bitPosi);
//	  else
//		LPC_GPIO2->IEV |= (0x1<<bitPosi);
//	break;
//	case PORT3:
//	  if ( sense == 0 )
//	  {
//		LPC_GPIO3->IS &= ~(0x1<<bitPosi);
//		/* single or double only applies when sense is 0(edge trigger). */
//		if ( single == 0 )
//		  LPC_GPIO3->IBE &= ~(0x1<<bitPosi);
//		else
//		  LPC_GPIO3->IBE |= (0x1<<bitPosi);
//	  }
//	  else
//	  	LPC_GPIO3->IS |= (0x1<<bitPosi);
//	  if ( event == 0 )
//		LPC_GPIO3->IEV &= ~(0x1<<bitPosi);
//	  else
//		LPC_GPIO3->IEV |= (0x1<<bitPosi);
//	break;
//	default:
//	  break;
//  }
//  return;


	if ( sense == 0 )
	{
		GPIO_port[portNum]->IS &= ~(1 << bitPosi);
		/* single or double only applies when sense is 0(edge trigger). */
		if ( single == 0 )
			GPIO_port[portNum]->IBE &= ~(1 << bitPosi);
		else
			GPIO_port[portNum]->IBE |= (1 << bitPosi);
	}
	else
		GPIO_port[portNum]->IS |= (1 << bitPosi);
	if ( event == 0 )
		GPIO_port[portNum]->IEV &= ~(1 << bitPosi);
	else
		GPIO_port[portNum]->IEV |= (1 << bitPosi);
}


/*****************************************************************************
** Function name:		GPIOIntEnable
**
** Descriptions:		Enable Interrupt Mask for a port pin.
**
** parameters:			port num, bit position
** Returned value:		None
**
*****************************************************************************/
void GPIO_IntEnable( uint32_t portNum, uint32_t bitPosi )
{
//  switch ( portNum )
//  {
//	case PORT0:
//	  LPC_GPIO0->IE |= (0x1<<bitPosi);
//	break;
// 	case PORT1:
//	  LPC_GPIO1->IE |= (0x1<<bitPosi);
//	break;
//	case PORT2:
//	  LPC_GPIO2->IE |= (0x1<<bitPosi);
//	break;
//	case PORT3:
//	  LPC_GPIO3->IE |= (0x1<<bitPosi);
//	break;
//	default:
//	  break;
//  }
//  return;

	GPIO_port[portNum]->IE |= (1 << bitPosi);
}


/*****************************************************************************
** Function name:		GPIOIntDisable
**
** Descriptions:		Disable Interrupt Mask for a port pin.
**
** parameters:			port num, bit position
** Returned value:		None
**
*****************************************************************************/
void GPIO_IntDisable( uint32_t portNum, uint32_t bitPosi )
{
//  switch ( portNum )
//  {
//	case PORT0:
//	  LPC_GPIO0->IE &= ~(0x1<<bitPosi);
//	break;
// 	case PORT1:
//	  LPC_GPIO1->IE &= ~(0x1<<bitPosi);
//	break;
//	case PORT2:
//	  LPC_GPIO2->IE &= ~(0x1<<bitPosi);
//	break;
//	case PORT3:
//	  LPC_GPIO3->IE &= ~(0x1<<bitPosi);
//	break;
//	default:
//	  break;
//  }
//  return;

	GPIO_port[portNum]->IE &= ~(1 << bitPosi);
}


/*****************************************************************************
** Function name:		GPIOIntStatus
**
** Descriptions:		Get Interrupt status for a port pin.
**
** parameters:			port num, bit position
** Returned value:		None
**
*****************************************************************************/
uint32_t GPIO_IntStatus( uint32_t portNum, uint32_t bitPosi )
{
//  uint32_t regVal = 0;
//
//  switch ( portNum )
//  {
//	case PORT0:
//	  if ( LPC_GPIO0->MIS & (0x1<<bitPosi) )
//		regVal = 1;
//	break;
// 	case PORT1:
//	  if ( LPC_GPIO1->MIS & (0x1<<bitPosi) )
//		regVal = 1;
//	break;
//	case PORT2:
//	  if ( LPC_GPIO2->MIS & (0x1<<bitPosi) )
//		regVal = 1;
//	break;
//	case PORT3:
//	  if ( LPC_GPIO3->MIS & (0x1<<bitPosi) )
//		regVal = 1;
//	break;
//	default:
//	  break;
//  }
//  return ( regVal );

	if((GPIO_port[portNum]->MIS) & (1 << bitPosi)) return 1;
	else return 0;
}


/*****************************************************************************
** Function name:		GPIOIntClear
**
** Descriptions:		Clear Interrupt for a port pin.
**
** parameters:			port num, bit position
** Returned value:		None
**
*****************************************************************************/
void GPIO_IntClear( uint32_t portNum, uint32_t bitPosi )
{
//  switch ( portNum )
//  {
//	case PORT0:
//	  LPC_GPIO0->IC |= (0x1<<bitPosi);
//	break;
// 	case PORT1:
//	  LPC_GPIO1->IC |= (0x1<<bitPosi);
//	break;
//	case PORT2:
//	  LPC_GPIO2->IC |= (0x1<<bitPosi);
//	break;
//	case PORT3:
//	  LPC_GPIO3->IC |= (0x1<<bitPosi);
//	break;
//	default:
//	  break;
//  }
//  return;

	GPIO_port[portNum]->IC |= (1 << bitPosi);
}

/******************************************************************************
**                            End Of File
******************************************************************************/
