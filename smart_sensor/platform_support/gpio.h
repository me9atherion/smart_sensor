/* GPIO платформы. Содержит функции для управления
 * ключами нагревателей, каналов измерения и т.п. */

#ifndef __GPIO_H
#define __GPIO_H

#include "config.h"
#include "LPC11xx.h"
#include <stdint.h>

/* Порты */
#define PORT0				0
#define PORT1				1
#define PORT2				2
#define PORT3				3


/* Светодиоды */
#define LEDS_CH_AMOUNT		3		// Общее количество
#define LED1_PORT			PORT1
#define LED1_PINn			9
#define LED2_PORT			PORT1
#define LED2_PINn			10
#define LED3_PORT			PORT0
#define LED3_PINn			11

#define LED1_ON()			GPIO_LEDctrl(0, 1)
#define LED2_ON()			GPIO_LEDctrl(1, 1)
#define LED3_ON()			GPIO_LEDctrl(2, 1)

#define LED1_OFF()			GPIO_LEDctrl(0, 0)
#define LED2_OFF()			GPIO_LEDctrl(1, 0)
#define LED3_OFF()			GPIO_LEDctrl(2, 0)


/* Ключи аналоговой части (нагреватели сенсоров, питание на сенсоры,
 * питание аналоговых цепей) */
#define EN_UHTR_CH_AMOUNT	SENSORS_ANALOG
#define EN_USNS_CH_AMOUNT	SENSORS_ANALOG

#define EN_UHTR1_PORTn		PORT2
#define EN_UHTR1_PINn		4
#define EN_USNS1_PORTn		PORT2
#define EN_USNS1_PINn		5

#define EN_UHTR2_PORTn		PORT2
#define EN_UHTR2_PINn		6
#define EN_USNS2_PORTn		PORT2
#define EN_USNS2_PINn		7

#define EN_UHTR3_PORTn		PORT2
#define EN_UHTR3_PINn		8
#define EN_USNS3_PORTn		PORT2
#define EN_USNS3_PINn		9

#define EN_AVCC_PORTn		PORT2
#define EN_AVCC_PINn		11

#define EN_AVCC_ON()		GPIO_SetValue(EN_AVCC_PORTn, EN_AVCC_PINn, 1)
#define EN_AVCC_OFF()		GPIO_SetValue(EN_AVCC_PORTn, EN_AVCC_PINn, 0)


/* Функции общего назначения для GPIO */
void GPIO_IRQHandler(void);
void GPIO_Init(void);
void GPIO_SetDir( uint32_t portNum, uint32_t bitPosi, uint32_t dir );
void GPIO_SetValue( uint32_t portNum, uint32_t bitPosi, uint32_t bitVal );
void GPIO_SetInterrupt( uint32_t portNum, uint32_t bitPosi, uint32_t sense,
		uint32_t single, uint32_t event );
void GPIO_IntEnable( uint32_t portNum, uint32_t bitPosi );
void GPIO_IntDisable( uint32_t portNum, uint32_t bitPosi );
uint32_t GPIO_IntStatus( uint32_t portNum, uint32_t bitPosi );
void GPIO_IntClear( uint32_t portNum, uint32_t bitPosi );

/* Специфичные GPIO ресурсы платформы */
void GPIO_UHTRctrl(uint32_t ch, uint32_t state);
void GPIO_USNSctrl (uint32_t ch, uint32_t state);
void GPIO_LEDctrl (uint32_t ch, uint32_t state);

#endif /* end __GPIO_H */
/*****************************************************************************
**                            End Of File
******************************************************************************/
