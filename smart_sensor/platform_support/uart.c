/* Кешированный UART для отладочных/рантаймовых сообщений */

#include "uart.h"


/* Буфер для кеширования передачи */
volatile uint8_t UART_TxBuffer[UART_TX_BUFFER_SIZE];	// Буфер передачи
volatile uint32_t UART_TxBufCachePtr;					// Данные для последующей передачи кешируются по этому указателю
volatile uint32_t UART_TxBufTransmitPtr;				// Передающий указатель, догоняет указатель кеширования
volatile uint32_t UART_TxBufCacheCnt;					// Счетчик кеша



/*===================================================================================
 * Обработчик прерывания UART
 *===================================================================================*/
void UART_IRQHandler(void) {

	uint8_t IIRValue;

	/* Читаеем Interrupt Identification Register для определения источника
	 * прерывания */
	IIRValue = LPC_UART->IIR;

	IIRValue >>= 1;			// Пропускаем общий бит наличия прерывания
	IIRValue &= 0x07;		// Выделяем интересующие 3 бита

	/* THRE, transmit holding register empty */
	if(IIRValue == IIR_THRE) {
		/* Прерывание по THRE. Проверяем статус и
		 * пишем с кеша, если есть что писать */
		if (LPC_UART->LSR & LSR_THRE) {

			if(UART_TxBufCacheCnt) {
				/* Есть кешированные данные на отправку. Проверяем конец буфера
				 * и передаем байт с кеша догоняя указатель кеширования */
				if(UART_TxBufTransmitPtr >= UART_TX_BUFFER_SIZE)
					UART_TxBufTransmitPtr = 0;
				LPC_UART->THR = UART_TxBuffer[UART_TxBufTransmitPtr];
				UART_TxBufTransmitPtr++;
				UART_TxBufCacheCnt--;		// Кеш уменьшился на 1 байт
			}
		}
	}
}


/*===================================================================================
 * Инит UARTа
 *===================================================================================*/
void UARTInit(uint32_t baudrate)
{
	uint32_t Fdiv;
	uint32_t regVal;

	NVIC_DisableIRQ(UART_IRQn);

	/* Обнуляем кеш передачи */
	UART_TxBufCachePtr = 0;
	UART_TxBufTransmitPtr = 0;
	UART_TxBufCacheCnt = 0;

	/* Настройка пина TX */
	LPC_IOCON->PIO1_7 &= ~(0x07 | (0x03 << 3));		// Подтяжки отключаем
	LPC_IOCON->PIO1_7 |= 0x01;     					// UART TXD функция

	/* Тактирование UART */
	LPC_SYSCON->SYSAHBCLKCTRL |= (1<<12);	// Включаем тактирование
	LPC_SYSCON->UARTCLKDIV = 0x1;     		// Прескалер UART

	LPC_UART->LCR = 0x83;             		// 8N1 формат, DLAB = 1

	/* Бодрейт */
	regVal = LPC_SYSCON->UARTCLKDIV;
	Fdiv = (((SystemCoreClock/LPC_SYSCON->SYSAHBCLKDIV)/regVal)/16)/baudrate ;
	LPC_UART->DLM = Fdiv / 256;
	LPC_UART->DLL = Fdiv % 256;

	LPC_UART->LCR = 0x03;		// DLAB = 0
	LPC_UART->FCR = 0x07;		// Разрешаем и сбрасываем FIFO

	/* Читаем LSR чтобы сбросить его */
	regVal = LPC_UART->LSR;

	/* Обеспечиваем чистый старт без данных в Tx или Rx FIFO */
	while ( (LPC_UART->LSR & (LSR_THRE|LSR_TEMT)) != (LSR_THRE|LSR_TEMT) );
	while ( LPC_UART->LSR & LSR_RDR ) {
		regVal = LPC_UART->RBR;	/* Dump data from RX FIFO */
	};

	/* Включаем канал прерывания UARTа */
	NVIC_EnableIRQ(UART_IRQn);

	/* Прерывание разрешается по пустому буферу передатчика */
	LPC_UART->IER = IER_THRE;
}


/*===================================================================================
 * Отправка одного байта через UART. Функция нужна для прямого вывода сообщений в
 * канал через стандарные или пользовательские функции вывода
 *===================================================================================*/
void UARTSendByte (const char byte) {

	/* Ждем свободное место в кеше */
	while(UART_TxBufCacheCnt >= UART_TX_BUFFER_SIZE);

	LPC_UART->IER &= ~IER_THRE;		// Атомарно манипулируем кешем

	if((LPC_UART->LSR & LSR_THRE) && !UART_TxBufCacheCnt) {
		/* Запуск передачи при условии пустого буфера передачи и отсутствия
		 * кешированных данных */
		LPC_UART->THR = byte;
	} else {
		/* В противном случае кешируем данные */
		if(UART_TxBufCachePtr >= UART_TX_BUFFER_SIZE)
			UART_TxBufCachePtr = 0;
		UART_TxBuffer[UART_TxBufCachePtr] = byte;
		UART_TxBufCachePtr++;
		UART_TxBufCacheCnt++;
	}

	LPC_UART->IER |= IER_THRE;		// Восстанавливаем прерывания от UART
}


/*===================================================================================
 * Отправляет N байт
 *===================================================================================*/
void UARTSendN(const void *src, uint32_t len) {

	const char *byte = src;

	while(len) {
		UARTSendByte(*byte);
		len--;
	}
}


/*===================================================================================
 * Отправляет NULL-terminated строку
 *===================================================================================*/
void UARTSendStr(const char *str) {

	while(*str != 0) {
		UARTSendByte(*str);
		str++;
	}
}
