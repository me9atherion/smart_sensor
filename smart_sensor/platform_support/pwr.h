/* Библиотека для управления тактированием и засыпанием ядра */

#ifndef PWR_H_
#define PWR_H_

#include <stdint.h>

enum clkout{
	CLK_IRC = 0,		// Internal RC osc (~12MHz)
	CLK_SYSOSC = 1,		// External system osc
	CLK_WDT = 2,		// Watchdog osc (~8KHz)
	CLK_MAIN = 3		// Main clock
};

/* Вывод частоты генератора наружу (для отладочных целей) */
void pwr_clkout(uint8_t enable, enum clkout source);

/* Тактирование от внешнего кварца на 12мГц через PLL */
void pwr_switch_to_48MHz(void);

/* Тактирование от внутреннего RC осциллятора на 12МГц */
void pwr_switch_to_12MHz_RC(void);

/* Тактирование от осциллятора вачдога ~9кГц */
void pwr_switch_to_9KHz(void);

/* Инит генератора вачдога для спящего режима и для самого вачдога.
 * Генератор работает на 9375Гц, дефолтный таймаут - 2с.
 * Для запуска вачдога его надо сбросить (после инита он стоит) */
void wdt_init(void);

/* Перезагрузка вачдога. Если периодически не вызывать,
 * система перезагрузится */
void wdt_feed (void);

/* Установка нового значения таймаута для вачдога и перезагрузка */
void wdt_new_timeout (uint16_t seconds);

/* Перевод девайса в глубокий сон */
void pwr_deepsleep(uint16_t _100_ms);

#endif /* PWR_H_ */
