/* Кешированный UART для отладочных/рантаймовых сообщений */

#ifndef __UART_H
#define __UART_H

#include <stdint.h>
#include "config.h"
#include "LPC11xx.h"


/* Биты в регистрах модуля UART */
#define IER_RBR		0x01
#define IER_THRE	0x02
#define IER_RLS		0x04

#define IIR_PEND	0x01
#define IIR_RLS		0x03
#define IIR_RDA		0x02
#define IIR_CTI		0x06
#define IIR_THRE	0x01

#define LSR_RDR		0x01
#define LSR_OE		0x02
#define LSR_PE		0x04
#define LSR_FE		0x08
#define LSR_BI		0x10
#define LSR_THRE	0x20
#define LSR_TEMT	0x40
#define LSR_RXFE	0x80


/* Инициализация интерфейса и настройка бодрейта */
void UARTInit(uint32_t Baudrate);

/* Передача одного байта по каналу UART для организации
 * форматированного вывода сообщений прямо в канал */
void UARTSendByte (char byte);

/* Передача строки */
void UARTSendStr(const char *str);

/* Передача N байт */
void UARTSendN(const void *src, uint32_t len);

#endif /* end __UART_H */
