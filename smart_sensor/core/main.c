
#ifdef __USE_CMSIS
#include "LPC11xx.h"
#endif

#include <cr_section_macros.h>

#include "config.h"
#include "uart.h"
#include "xprintf.h"
#include "systick.h"
#include "pwr.h"
#include "gpio.h"
#include "i2c_slave.h"

/* Версия ПО */
const uint8_t fw_version_major = 0;
const uint8_t fw_version_minor = 1;


/*===================================================================================
 * Инит после системной кнфигурации - вызова SystemInit()
 *===================================================================================*/
void primary_init (void) {

#if WATCHDOG_ENABLE
#if !(WATCHDOG_EARLY_INIT)
	/* Запускаем вачдог. Теперь надо его периодически
	 * "кормить" чтобы система не перезагрузилась */
	wdt_init();
	wdt_feed();
#else
	/* SystemInit() пишет дефолтное значение в регистр тактирования, так что
	 * надо включить тактирование на вачдог обратно */
	LPC_SYSCON->SYSAHBCLKCTRL |= (1 << 15);
#endif
#endif

#if BOD_ENABLE
#if !(BOD_EARLY_INIT)
	/* Включаем BOD и разрешаем сброс системы по просадке напряжения */
	LPC_SYSCON->PDRUNCFG &= ~(1 << 3);						// Питание BOD
	LPC_SYSCON->BODCTRL &= ~( (0x3 << 0) | (0x3 << 2) );
	LPC_SYSCON->BODCTRL |= (0x3 << 0) | (1 << 4);			// BODRESETLEV = 2.63V & BOD reset enable
#endif
#endif

	/* Обновляем значение системной частоты */
	SystemCoreClockUpdate();

	/* Счетчик системных тиков */
	systickInit();

	/* GPIO */
	GPIO_Init();

	/* Настройка UART и привязка к библиотеке форматированного
	 * вывода сообщений xprintf */
	UARTInit(UART_BAUDRATE);
	xdev_out(UARTSendByte);

	/* I2C эмулятор ведомого устройства */
	I2C_init();
}


/*===================================================================================
 * main()
 *===================================================================================*/
int main(void) {

	volatile uint32_t t, n = 0;

	primary_init();

	/* Стартовая информация */
	xputs("\n\n\n\n"
			"===========================================\n"
			"SMART-SENSOR plarform startup!\n");
	xprintf("Firmware Version: %u.%u\n"
			"Build Date: %s\n"
			"Build Time: %s\n",
			fw_version_major, fw_version_minor,
			__DATE__, __TIME__);

	xputs("Core reset sources: ");
	for(uint8_t bit = 0; bit < 5; bit ++) {
		switch(LPC_SYSCON->SYSRSTSTAT & (1 << bit)) {
		case (1 << 0): /* POR*/
				xputs("POR "); break;

		case (1 << 1): /* EXTRST */
				xputs("EXTRST "); break;

		case (1 << 2): /* WDT */
				xputs("WDT "); break;

		case (1 << 3): /* BOD */
				xputs("BOD "); break;

		case (1 << 4): /* SYSRST */
				xputs("SYSRST "); break;

		default:
			break;
		}
	}
	/* Запись 1 чистит флаг источника сброса. Чистим все источники, вызвавшие
	 * сброс системы */
	LPC_SYSCON->SYSRSTSTAT = LPC_SYSCON->SYSRSTSTAT;
	xputs("\n"
			"===========================================\n"
			"\n\n");


	xputs_ts("Message end\n");

	t = systickGetTicks();

	uint32_t channel = 0, state = 1;

	for(;;) {

		wdt_feed();


		GPIO_UHTRctrl(channel, state);
		GPIO_USNSctrl(channel, state);
		GPIO_LEDctrl(channel, state);


		if(systickCalculateDelay_ms(t) >= 37) {
			t = systickGetTicks();
			n++;
			xprintf_ts("Iteration: %u\n", n);
		}
	}



    return 0 ;
}
