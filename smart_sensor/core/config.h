/* Smart-sensor platform configuration.
 * This file contains all configurations for platform. */

#ifndef CONFIG_H_
#define CONFIG_H_

#include "i2c_slave.h"

/*
 * Sensors configuration section for smart-sensor platform
 */

#define SENSORS_ANALOG				3			// Analog channels amount allowed on current platform
#define SENSORS_DIGITAL				9			// Digital channels amount allowed on current platform

#if ((SENSORS_ANALOG + SENSORS_DIGITAL) > (12)) || \
	((SENSORS_ANALOG + SENSORS_DIGITAL) == 0)
#error "Wrong sensors configuration! Check sensor configuration section."
#endif


/*
 * I2C bus configuration section
 */
#define I2C_MIN_ADDRESS				0x00		// Defines lowest smart-sensor address on I2C bus


/*
 * UART configuration
 */
#define UART_BAUDRATE 				115200		// Defines baudrate for debug UART port
#define UART_TX_BUFFER_SIZE			64			// TX buffer size for UART [bytes] (speeds up transmission handling)


/*
 * System config
 */
#define WATCHDOG_ENABLE				0			// Watchdog: 1 - enable, 0 - disable
#define WATCHDOG_EARLY_INIT			1			// Initialization watchdog: 1 - prior to SystemInit(), 0 - after
#define BOD_ENABLE					1			// Brown Out Detector: 1 - enable, 0 - disable. BOD will cause system reset
#define BOD_EARLY_INIT				1			// BOD initialization: 1 - prior to SystemInit(), 0 - after
#define BOD_DURING_SLEEP_ENABLE		1			// BOD during sleep causes additioanal current draw: 1 - enable, 0 - disable

#ifndef DEBUG
#if !(WATCHDOG_ENABLE)
#warning "Watchdog must be enabled. Turn off only for debug!"
#endif

#if !(BOD_ENABLE)
#warning "BOD must be anbled. Turn off only for debug!"
#endif
#endif

/*
 * Debug
 */
#ifdef DEBUG

#define DEBUG_RAM_FILL_AA			0				// 1 - fills RAM with 0xAA on startup (for max stack usage detection)
#define DEBUG_RAM_FILL_BEGIN		0x10000000UL	// Start address for filling
#define DEBUG_RAM_FILL_SIZE			0x2000			// Full RAM lenght (end of fill)
#define DEBUG_RAM_FILL_MARGIN		128				// Margin upon RAM end to skip filling to preserve corruption of current stack

#define XPRINTF_SUPRESS				0				// 1 - forces disabling of xprintf(), xputs(), xprintf_ts(), xputs_ts()
#define XPRINTF_USE_TIMESTAMP		1				// 1 - adds time prefix generation with xprintf_ts(), xputs_ts()

#else
#endif


#endif /* CONFIG_H_ */
